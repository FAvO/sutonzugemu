package de.favo.v2.sutonzugemu.logic;

import de.favo.v2.sutonzugemu.common.Point;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;

class LogicTest {

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void exchange() {
        int[][] game = new int[][]{ {1,1,1,2,3,1,5,6},
                                    {1,2,3,4,5,3,6,1},
                                    {1,1,1,2,3,4,5,6},
                                    {3,4,3,1,1,4,3,4},
                                    {0,4,6,2,1,0,0,3},
                                    {5,4,6,3,4,4,5,1},
                                    {2,5,0,2,3,6,4,3},
                                    {1,2,4,6,6,5,1,3},
        };
        int[][] oGame = deepArrayCopy(game);
        int[][] g1, g2, g3, g4;
        printField(oGame, oGame);
        Logic l = new Logic(game);
        g1 = deepArrayCopy(l.getGame());
        printField(g1,oGame);
       /* l.exchange(new Point(7,0), new Point(7,1));
        g2 = deepArrayCopy(l.getGame());
        printField(g2,g1);
        l.exchange(new Point(5,6), new Point(5,7));
        printField(l.getGame(),g1);*/
        l.exchange(new Point(4,1), new Point(5,1));
        g2 = deepArrayCopy(l.getGame());
        printField(g2, g1);
        //l.exchange(new Point(6,1), new Point(7,1));
        GameEvent[][] data = l.exchange(new Point(7,4), new Point(7,5));
        printField(l.getGame(), g2);
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.println(data[i][j].toString());
            }
        }
    }

    public static void printField(int[][] game, int [][] oGame) {
        System.out.println("========");
        for (int y = 0; y < Logic.FIELD_SIZE; y++) {
            for (int x = 0; x < Logic.FIELD_SIZE; x++) {
                if (game[y][x] != oGame[y][x])
                    System.out.print("-" + game[y][x] + "-");
                else
                    System.out.print(" " + game[y][x] + " ");
            }
            System.out.println();
        }
    }

    @Test
    void getPossibleMoves() {
    }

    /**
     * Returns a copy of the specified array object, deeply copying
     * multidimensional arrays. If the specified object is null, the
     * return value is null. Note: if the array object has an element
     * type which is a reference type that is not an array type, the
     * elements themselves are not deep copied. This method only copies
     * array objects.
     *
     * @param  array the array object to deep copy
     * @param  <T>   the type of the array to deep copy
     * @return a copy of the specified array object, deeply copying
     *         multidimensional arrays, or null if the object is null
     * @throws IllegalArgumentException if the specified object is not
     *                                  an array
     */
    public static <T> T deepArrayCopy(T array) {
        if (array == null)
            return null;

        Class<?> arrayType = array.getClass();
        if (!arrayType.isArray())
            throw new IllegalArgumentException(arrayType.toString());

        int length = Array.getLength(array);
        Class<?> componentType = arrayType.getComponentType();

        @SuppressWarnings("unchecked")
        T copy = (T) Array.newInstance(componentType, length);

        if (componentType.isArray()) {
            for (int i = 0; i < length; ++i)
                Array.set(copy, i, deepArrayCopy(Array.get(array, i)));
        } else {
            System.arraycopy(array, 0, copy, 0, length);
        }

        return copy;
    }
}