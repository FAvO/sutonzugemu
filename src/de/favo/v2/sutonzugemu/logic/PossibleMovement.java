package de.favo.v2.sutonzugemu.logic;

import de.favo.v2.sutonzugemu.common.Point;

/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.Arrays;


/**Is a free(possible) move in a KamikuLogic playing field
 * @see Logic*/
public class PossibleMovement {

	private final Point[] move;
	private final Point change1;
	private final Point change2;
	
	public PossibleMovement(Point[] points, Point tile1, Point tile2) {
		change1 = tile1;
		change2 = tile2;
		move = points;
	}
	/**Checks if Vector <code>possibleMovementVector</code> contains Point <code>point</code> as (any) part of a free move.*/
	public static boolean isInPossibleMovementIterator(ArrayList<PossibleMovement> possibleMovementVector, Point point) {
		for (PossibleMovement entry:possibleMovementVector)
			if (entry.contains(point))
				return true;
		return false;
	}
	/**
	 * @return the move
	 */
	public Point[] getMove() {
		return move;
	}

	/**
	 * @return the position where the piece to move is
	 */
	public Point getChangeFrom() {
		return change1;
	}

	/**
	 * @return the position where you have to set
	 */
	public Point getChangeTo() {
		return change2;
	}

	/**Checks if this <code>FreeMove</code> contains the Point <code>p</code>.*/
	public boolean contains(Point p){
		return getChangeTo().equals(p) || getChangeFrom().equals(p) || Arrays.asList(getMove()).contains(p);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FreeMove [move=" + Arrays.toString(move) + ", change1="
				+ change1 + ", change2=" + change2 + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((change1 == null) ? 0 : change1.hashCode());
		result = prime * result + ((change2 == null) ? 0 : change2.hashCode());
		result = prime * result + Arrays.hashCode(move);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PossibleMovement other = (PossibleMovement) obj;
		if (change1 == null) {
			if (other.change1 != null)
				return false;
		} else if (!change1.equals(other.change1))
			return false;
		if (change2 == null) {
			if (other.change2 != null)
				return false;
		} else if (!change2.equals(other.change2))
			return false;
		return Arrays.equals(move, other.move);
	}
}
