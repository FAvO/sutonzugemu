package de.favo.v2.sutonzugemu.logic;

/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import de.favo.v2.sutonzugemu.common.Point;

import java.util.*;

/** this class describes the data view of the game */
public class Logic {
    public static final int FIELD_SIZE = 8;
    public static final int AMOUNT_GAME_PIECES = 7;
    public static final int[] SCORES = new int[]{0,0,0,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150};
    private final int[][] game;
    private int score;

    public Logic() {
        this.game       = new int[FIELD_SIZE][FIELD_SIZE];
        this.score      = 0;
        fillBoard(true);
        do {
            fillBoard(false);
            deleteTilesToBeRemoved(determineTilesToBeRemoved());
            wiggleBoard();
        } while (hasEmptySlots());
    }

    public Logic(int[][] game) {
        this.game       = game;
        this.score      = 0;
        do {
            fillBoard(false);
            deleteTilesToBeRemoved(determineTilesToBeRemoved());
            wiggleBoard();
        } while (hasEmptySlots());
    }

    public int[][] getGame() {
        return game;
    }

    private GameEvent[][] fillBoard(boolean flush){
        Random r = new Random();
        ArrayList<GameEvent> events = new ArrayList<>();
        for (int x = 0; x < game.length; x++){
            for (int y = 0; y < game[0].length; y++){
                // sets a random game piece to the coordinates x and y by selecting a random tile
                if (flush || game[y][x] == -1) {
                    game[y][x] = r.nextInt(AMOUNT_GAME_PIECES);
                    events.add(new GameEvent(GameEventType.NEW_TILE_ADDED, new Point(x,y), game[y][x]));
                }
            }
        }
        return new GameEvent[][]{events.toArray(new GameEvent[0])};
    }

    public GameEvent[][] exchange(Point firstTile, Point secondTile) {
        GameEvent[][] tilesToBeDeleted;
        GameState technicalMove;
        ArrayList<GameEvent[]> events = new ArrayList<>();

        technicalMove = moveTilesToBeRemoved(firstTile, secondTile);
        events.add(new GameEvent[]{
                new GameEvent(GameEventType.TILES_EXCHANGED, firstTile, secondTile)});

        if (technicalMove != GameState.FULFILLED) { // failed to do so!
            return new GameEvent[0][];
        }

        while ( (tilesToBeDeleted = determineTilesToBeRemoved()).length != 0 ) {
            Collections.addAll(events, tilesToBeDeleted);

            for (GameEvent[] ge : tilesToBeDeleted) {   // score
                score = score + SCORES[ge.length];
                events.add(new GameEvent[]{new GameEvent(GameEventType.NEW_SCORE_REACHED, score)});
            }
            deleteTilesToBeRemoved(tilesToBeDeleted);
            Collections.addAll(events, wiggleBoard());
            Collections.addAll(events, fillBoard(false));
        }

        return events.toArray(new GameEvent[0][]);
    }

    private void deleteTilesToBeRemoved(GameEvent[][] events) {
        Point tbd;
        for (GameEvent[] events1:events) {
            for (GameEvent event: events1) {
                if (event.getReason() == GameEventType.TILE_REMOVED) {
                  tbd = event.getPoint1();
                  this.game[tbd.y()][tbd.x()] = -1;
                }
            }
        }
    }

    private GameEvent[][] wiggleBoard() {
        ArrayList<GameEvent> events = new ArrayList<>();
        int i;
        for (int x = 0; x < FIELD_SIZE; x++) {
            for (int y = FIELD_SIZE - 1; y >= 0; y--) {
                if (game[y  ][x] == -1 && y > 0) {
                    i = 1;
                    while (y - i > 0 && game[y-i][x] == -1) { i++; }
                    if (game[y-i][x] != -1) {
                        game[y][x] = game[y - i][x];
                        game[y - i][x] = -1;
                        events.add(new GameEvent(GameEventType.TILE_MOVED_TO, new Point(x, y - i), new Point(x, y)));
                    }
                }
            }
        }
        return new GameEvent[][]{events.toArray(new GameEvent[0])};
    }
    private GameState moveTilesToBeRemoved(Point start, Point end){
        ArrayList<PossibleMovement> possibleMoves = getPossibleMoves();

        if ((Math.abs(start.x() - end.x()) != 1) == (Math.abs(start.y() - end.y()) != 1)){
            return GameState.MOVE_IMPOSSIBLE;
        }
        if (!
                ((PossibleMovement.isInPossibleMovementIterator(possibleMoves, start) && (PossibleMovement.isInPossibleMovementIterator(possibleMoves, end))))){
            if (possibleMoves.size() > 0)
                return GameState.NO_FREE_MOVE_HERE;
            else
                return GameState.GAME_OVER;
        }

        int ch1 = game[start.y()][start.x()];
        int ch2 = game[  end.y()][  end.x()];

        game[  end.y()][  end.x()] = ch1;
        game[start.y()][start.x()] = ch2;

        return GameState.FULFILLED;
    }

    private GameEvent[][] determineTilesToBeRemoved() {
        ArrayList<GameEvent[]> deleteEvents = new ArrayList<>();
        ArrayList<Point[]> toBeEliminated = new ArrayList<>(10);

        int lastPoint, correctTiles;
        Point[] points;
        Point point;

        // find 3 or more in a row and mark them for deletion
        for (int y = 0; y < FIELD_SIZE; y++){           // row à row
            lastPoint       = -1;
            correctTiles    =  0;
            for (int x = 0; x < FIELD_SIZE; x++){
                if (game[y][x] == -1) {lastPoint = -1; continue;}
                if (game[y][x] == lastPoint) {  correctTiles++; }
                if (x + 1 < FIELD_SIZE && game[y][x+1] != game[y][x] || x + 1 == FIELD_SIZE) {
                    if (correctTiles >= 2) { // 2 following identical tiles!
                        points = new Point[correctTiles + 1];
                        for (int i = correctTiles; i >= 0; i--) {points[correctTiles-i] = new Point(x-i,y);}
                        toBeEliminated.add(points);
                    }
                    correctTiles = 0;
                }
                lastPoint = game[y][x];
            }
        }
        // find 3 or more in a column and mark them for deletion
        for (int x = 0; x < FIELD_SIZE; x++){
            lastPoint       = -1;
            correctTiles    =  0;
            for (int y = 0; y < FIELD_SIZE; y++){
                if (game[y][x] == -1) {lastPoint = -1; continue;}
                if (game[y][x] == lastPoint) {  correctTiles++; }
                if (y + 1 < FIELD_SIZE && game[y+1][x] != game[y][x] || y + 1 == FIELD_SIZE) {
                    if (correctTiles >= 2) {
                        points = new Point[correctTiles + 1];
                        for (int i = correctTiles; i >= 0; i--) {points[correctTiles-i] = new Point(x,y-i);}
                        toBeEliminated.add(points);
                    }
                    correctTiles = 0;
                }
                lastPoint = game[y][x];
            }
        }
        // find 3-pair (or bigger) tiles which are connected
        HashSet<List<Integer>> eliminationList = new HashSet<>();
        for (int i = 0; i < toBeEliminated.size(); i++) {                   // outer loop (toBeEliminated)
            List<Integer> shouldBeEliminatedAsOne = new ArrayList<>();
            shouldBeEliminatedAsOne.add(i);

            for (int k = 0; k < toBeEliminated.get(i).length; k++) {  // first inner loop (Points of entry in toBeEliminated)
                point = toBeEliminated.get(i)[k];
                for (int j = 0; j < toBeEliminated.size(); j++) {
                    if (i == j) {continue;}
                    if (Arrays.asList(toBeEliminated.get(j)).contains(point)) {
                        shouldBeEliminatedAsOne.add(j);
                    }
                }
            }
            eliminationList.add(shouldBeEliminatedAsOne);
        }
        // merge all connected 3-pair tiles
        for (List<Integer> entries:eliminationList.stream().toList()) {
            ArrayList<GameEvent> eventsInAMove = new ArrayList<>(6);
            for (Integer i:entries) {
                for (Point tbDeleted:toBeEliminated.get(i)) {
                    eventsInAMove.add(new GameEvent(GameEventType.TILE_REMOVED, tbDeleted));
                }
            }
            deleteEvents.add(eventsInAMove.toArray(new GameEvent[0]));
        }
        return deleteEvents.toArray(new GameEvent[0][]);
    }

    public boolean isMovePossible(Point point1, Point point2) {
        ArrayList<PossibleMovement> movements = getPossibleMoves();
        for (PossibleMovement possibleMovement:movements) {
            if (possibleMovement.getChangeFrom().equals(point1) && possibleMovement.getChangeTo().equals(point2) ||
                possibleMovement.getChangeFrom().equals(point2) && possibleMovement.getChangeTo().equals(point1))
                return true;

        }
        return false;
    }
    public ArrayList<PossibleMovement> getPossibleMoves(){
        ArrayList<PossibleMovement> freeMoves = new ArrayList<>(20);
        for (int x = 0; x < FIELD_SIZE; x++){
            for (int y = 0; y < FIELD_SIZE; y++){
                {
                    // 16 if clauses in general...
                    // HORIZONTAL

                    /* xx xx
                     * X X X
                     * xxxxx*/
                    if (x+2 < FIELD_SIZE &&
                            y-1 >= 0 &&
                            ( game[y][x] == game[y-1][x+1]) && (game[y][x+2] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+1,y-1),
                                                new Point(x+2,y)
                                        },
                                        new Point(x+1,y-1),new Point(x+1,y)));
                    }
                    /* xxxxx
                     * X X X
                     * xx xx*/
                    if (x+2 < FIELD_SIZE &&
                            y+1 < FIELD_SIZE &&
                            ( game[y][x] == game[y+1][x+1]) && (game[y][x+2] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+1,y+1),
                                                new Point(x+2,y)
                                        },
                                        new Point(x+1,y+1),new Point(x+1,y)));
                    }

                    /*xxx X
                     *X  xx
                     *xxxxx*/
                    if (x+2 < FIELD_SIZE &&
                            y-1 >= 0 &&
                            ( game[y][x] == game[y][x+1]) && (game[y-1][x+2] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+1,y),
                                                new Point(x+2,y-1)
                                        },
                                        new Point(x+2,y-1),new Point(x+2,y)));
                    }
                    /*xxxxx
                     *X  xx
                     *xxx X*/
                    if (x+2 < FIELD_SIZE &&
                            y+1 < FIELD_SIZE &&
                            ( game[y][x] == game[y][x+1]) && (game[y+1][x+2] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+1,y),
                                                new Point(x+2,y+1)
                                        },
                                        new Point(x+2,y+1),new Point(x+2,y)));
                    }
                    /*xxxxx
                     *X xxx
                     *xx  X*/
                    if (x+2 < FIELD_SIZE &&
                            y+1 < FIELD_SIZE &&
                            ( game[y][x] == game[y+1][x+1]) && (game[y+1][x+2] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+1,y+1),
                                                new Point(x+2,y+1)
                                        },
                                        new Point(x,y),new Point(x,y+1)));
                    }
                    /*xx  X
                     *X xxx
                     *xxxxx*/
                    if (x+2 < FIELD_SIZE &&
                            y-1 >= 0 &&
                            ( game[y][x] == game[y-1][x+1]) && (game[y-1][x+2] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+1,y-1),
                                                new Point(x+2,y-1)
                                        },
                                        new Point(x,y),new Point(x,y-1)));
                    }
                    /*xxxxx
                     *X  x
                     *xxxxx*/
                    if (x+3 < FIELD_SIZE &&
                            ( game[y][x] == game[y][x+1]) && (game[y][x+3] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+1,y),
                                                new Point(x+2,y)
                                        },
                                        new Point(x+3,y),new Point(x+2,y)));
                    }
                    /*xxxxx
                     *X x
                     *xxxxx*/
                    if (x+3 < FIELD_SIZE &&
                            ( game[y][x] == game[y][x+2]) && (game[y][x+3] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+2,y),
                                                new Point(x+3,y)
                                        },
                                        new Point(x,y),new Point(x+1,y)));
                    }
                }{
                    //VERTICAL... ... the next 8 if-clauses...
                    /* xx xx
                     * X xxx
                     * xx xx*/
                    if (x-1 >= 0 &&
                            y+2 < FIELD_SIZE &&
                            ( game[y][x] == game[y+1][x-1]) && (game[y+2][x] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x-1,y+1),
                                                new Point(x,y+2)
                                        },
                                        new Point(x-1,y+1),new Point(x,y+1)));
                    }

                    /* xx xx
                     * xxx X
                     * xx xx*/
                    if (y+2 < FIELD_SIZE &&
                            x+1 < FIELD_SIZE &&
                            ( game[y][x] == game[y+1][x+1]) && (game[y+2][x] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+1,y+1),
                                                new Point(x,y+2)
                                        },
                                        new Point(x+1,y+1),new Point(x,y+1)));
                    }
                    /*xx xx
                     *xx xx
                     *xxx X*/
                    if (x+1 < FIELD_SIZE &&
                            y+2 < FIELD_SIZE &&
                            ( game[y][x] == game[y+1][x]) && (game[y+2][x+1] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x,y+1),
                                                new Point(x+1,y+2)
                                        },
                                        new Point(x+1,y+2),new Point(x,y+2)));
                    }
                    /*xx xx
                     *xx xx
                     *X xxx*/
                    if (x-1 >= 0 &&
                            y+2 < FIELD_SIZE &&
                            ( game[y][x] == game[y+1][x]) && (game[y+2][x-1] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x,y+1),
                                                new Point(x-1,y+2)
                                        },
                                        new Point(x-1,y+2),new Point(x,y+2)));
                    }
                    /*xx xx
                     *X xxx
                     *X xxx*/
                    if (x-1 >= 0 &&
                            y+2 < FIELD_SIZE &&
                            ( game[y][x] == game[y+1][x-1]) && (game[y+2][x-1] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x-1,y+1),
                                                new Point(x-1,y+2)
                                        },
                                        new Point(x,y),new Point(x-1,y)));
                    }
                    /*xx xx
                     *xxx X
                     *xxx X*/
                    if (x+1 < FIELD_SIZE &&
                            y+2 < FIELD_SIZE &&
                            ( game[y][x] == game[y+1][x+1]) && (game[y+2][x+1] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x+1,y+1),
                                                new Point(x+1,y+2)
                                        },
                                        new Point(x,y),new Point(x+1,y)));
                    }
                    /*xx xx
                     *xx xx
                     *xxxxx
                     *xx xx
                     *xxxxx*/
                    if (y+3 < FIELD_SIZE &&
                            ( game[y][x] == game[y+1][x]) && (game[y+3][x] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x,y+1),
                                                new Point(x,y+3)
                                        },
                                        new Point(x,y+3),new Point(x,y+2)));
                    }
                    /*xx xx
                     *xxxxx
                     *xx xx
                     *xx xx
                     *xxxxx*/
                    if (y+3 < FIELD_SIZE &&
                            ( game[y][x] == game[y+2][x]) && (game[y+3][x] == game[y][x])){
                        freeMoves.add(
                                new PossibleMovement(
                                        new Point[]{
                                                new Point(x,y),
                                                new Point(x,y+2),
                                                new Point(x,y+3)
                                        },
                                        new Point(x,y),new Point(x,y+1)));
                    }
                }

            }//END of X loop
        }//END of y loop
        return freeMoves;
    }

    private boolean hasEmptySlots() {
        for (int x = 0; x < FIELD_SIZE; x++) {
            for (int y = 0; y < FIELD_SIZE; y++) {
                if (game[y][x] == -1) {
                    return true;
                }
            }
        }
        return false;
    }
}
