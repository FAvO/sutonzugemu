package de.favo.v2.sutonzugemu.logic;

/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import de.favo.v2.sutonzugemu.common.Point;

public class GameEvent {
    public static String[] COLOR_CODES = {"blue", "teal", "yellow", "green", "orange", "dark-green", "red", "purple"};
    private final GameEventType reason;
    private Point point1;
    private Point point2;
    private int color = -1;

    private int score;
    public GameEvent(GameEventType reason, Point point1, int color) {
        this.reason = reason;
        this.point1 = point1;
        this.color = color;
    }

    public GameEvent(GameEventType reason, Point point1) {
        this.reason = reason;
        this.point1 = point1;
    }

    public GameEvent(GameEventType reason, int newScore) {
        this.reason = reason;
        this.score = newScore;
    }
    public GameEvent(GameEventType reason, Point point1, Point point2) {
        this.reason = reason;
        this.point1 = point1;
        this.point2 = point2;
    }

    public Point getPoint1() {
        return point1;
    }

    public GameEventType getReason() {
        return reason;
    }

    public Point getPoint2() {
        return point2;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "GameEvent{" +
                "reason=" + reason +
                ", point1=" + toNullString(point1) +
                ", point2=" + toNullString(point2) +
                ", score=" + score +
                ", color=" + ( color > -1 && color < COLOR_CODES.length ? COLOR_CODES[color] : color) +
                '}';
    }
    private String toNullString(Object object) {
        if (object == null)
            return "null";
        return object.toString();
    }

    public int getColor() {
        return color;
    }
}
