package de.favo.v2.sutonzugemu.swingui;

/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import de.favo.v2.sutonzugemu.common.Point;
import de.favo.v2.sutonzugemu.common.PrecisePoint;

public class Tile {

    private final int color;
    private Point location;
    private Point target;
    private int progress = -1;
    private int animationLimit = -1;
    private State state;
    public Tile (int color, Point location) {
        this.color = color;
        this.location = location;
        this.state = State.DEFAULT;
        assert this.location != null;
    }

    public int getColor() {
        return color;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point point) {
        this.location = point;
    }

    public void animateToLocation(Point target) {
        this.state = State.ANIMATED;
        this.target = target;
        this.progress = 1;
        this.animationLimit = (int) Math.ceil(this.location.euclideanDistanceTo(target) * SettingsProvider.animationSpeed() * SettingsProvider.getFramesPerSecond());
    }

    public PrecisePoint getNextFrame() {
        PrecisePoint ret;
        if (this.state != State.ANIMATED)
            throw new IllegalStateException();

        if (this.progress < this.animationLimit) {
            ret = new PrecisePoint(
                    (this.location.x() - ((double) (this.location.x() - this.target.x())) * ((double) progress) / ((double) animationLimit)),
                    (this.location.y() - ((double) (this.location.y() - this.target.y())) * ((double) progress) / ((double) animationLimit))
            );
            progress++;
            return ret;
        } else {
            this.animationLimit = this.progress = -1;
            this.state = State.DEFAULT;
            this.location = new Point(this.target.x(), this.target.y());
            this.target = null;
            return this.location.toPrecisePoint();
        }
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    enum State {
        DEFAULT,
        FOCUSED,
        ANIMATED,
        DELETED
    }
}


