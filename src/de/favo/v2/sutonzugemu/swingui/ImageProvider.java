package de.favo.v2.sutonzugemu.swingui;

/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import de.favo.v2.sutonzugemu.logic.Logic;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class ImageProvider {
    private static final String BACKGROUND_IMAGE = "background.jpg";
    private static final String[] COLOR_IMAGES = {
            "colors/0.png",
            "colors/1.png",
            "colors/2.png",
            "colors/3.png",
            "colors/4.png",
            "colors/5.png",
            "colors/6.png",
            "colors/7.png"};
    private final String style;
    private BufferedImage[] originalTiles;
    private BufferedImage[] tiles;
    private BufferedImage[] focusedTiles;
    private BufferedImage background;

    public ImageProvider(String style){
        this.style = style;
    }

    public void loadImages(Dimension gameSize) {
        focusedTiles = new BufferedImage[Logic.FIELD_SIZE];
        originalTiles = new BufferedImage[Logic.FIELD_SIZE];
        tiles = new BufferedImage[Logic.FIELD_SIZE];
        try {
            background = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(style + "/" + BACKGROUND_IMAGE)));
            for (int i = 0; i < Logic.FIELD_SIZE; i++) {
                originalTiles[i] = ImageIO.read((Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(style + File.separator + COLOR_IMAGES[i]))));
            }
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }
        prepareImages(gameSize);
    }

    protected void prepareImages(Dimension gameSize) {
        BufferedImage buff;
        Graphics g;
        try {// normal tiles
            for (int i = 0; i < Logic.FIELD_SIZE; i++) {
                buff = new BufferedImage(
                        SettingsProvider.getTileSpaceWidth(gameSize),
                        SettingsProvider.getTileSpaceHeight(gameSize),
                        BufferedImage.TYPE_INT_ARGB);
                g = buff.getGraphics();
                g.drawImage(originalTiles[i],
                        SettingsProvider.getTilePaddingSize(gameSize),
                        SettingsProvider.getTilePaddingSize(gameSize),
                        SettingsProvider.getTileSpaceWidth(gameSize)  - SettingsProvider.getTilePaddingSize(gameSize) * 2,
                        SettingsProvider.getTileSpaceHeight(gameSize) - SettingsProvider.getTilePaddingSize(gameSize) * 2,
                        null);
                tiles[i] = buff;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        // focused tiles
        for (int i = 0; i < Logic.FIELD_SIZE; i++) {
            buff = new BufferedImage(
                    SettingsProvider.getTileSpaceWidth(gameSize),
                    SettingsProvider.getTileSpaceHeight(gameSize),
                    BufferedImage.TYPE_INT_ARGB);
            g = buff.getGraphics();
            g.drawImage(tiles[i],0,0,null);
            g.setColor(new Color(Color.BLACK.getRed(), Color.BLACK.getGreen(), Color.BLACK.getBlue(), 50));
            g.fillRoundRect(0,0,
                    SettingsProvider.getTileSpaceWidth(gameSize),
                    SettingsProvider.getTileSpaceHeight(gameSize),
                    4,4);
            focusedTiles[i] = buff;
        }
    }

    public BufferedImage getTile(Tile.State state, int color) {
        if (state == null || state.equals(Tile.State.DEFAULT) || state.equals(Tile.State.ANIMATED))
            return tiles[color];
        if (state.equals(Tile.State.FOCUSED))
            return focusedTiles[color];
        return null;
    }

    public BufferedImage getBackground() {
        return background;
    }
}

