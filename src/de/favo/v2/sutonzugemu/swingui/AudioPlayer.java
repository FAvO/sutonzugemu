package de.favo.v2.sutonzugemu.swingui;

/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Random;

import javax.sound.sampled.LineEvent.Type;

public class AudioPlayer {
    protected String[] songs = new String[]{"Art_Of_Silence.wav"};
    private Clip audioClip;
    private long pauseTime = -1;

    protected void loadClip(InputStream inputStream) throws LineUnavailableException, IOException, UnsupportedAudioFileException {
        AudioInputStream audioStream = AudioSystem.getAudioInputStream(new BufferedInputStream(inputStream));
        AudioFormat format = audioStream.getFormat();
        DataLine.Info info = new DataLine.Info(Clip.class, format);
        audioClip = (Clip) AudioSystem.getLine(info);
        audioClip.open(audioStream);
        audioClip.addLineListener(lineEvent -> {
            if (lineEvent.getType().equals(Type.START)) {
            } else if (lineEvent.getType().equals(Type.OPEN)) {
            } else if (lineEvent.getType().equals(Type.STOP)) {
            } else if (lineEvent.getType().equals(Type.CLOSE)) {
            }
        });
    }

    private InputStream getNextStream() {
        Random r = new Random();
        return Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("default/sounds/" + songs[r.nextInt(songs.length)]));
    }

    public void play() throws AudioPlaybackFailedException {
        try {
            loadClip(getNextStream());
            audioClip.start();
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            throw new AudioPlaybackFailedException();
        }

    }
    public void stop() {
        if (audioClip != null) {
            audioClip.stop();
            audioClip.close();
            audioClip = null;
        }
    }

    public void play_pause() {
        System.out.println("play pause");
        if (audioClip != null && audioClip.isRunning()) {
            pauseTime = audioClip.getMicrosecondLength();
            audioClip.stop();
        } else if (audioClip != null && !audioClip.isRunning()) {
            System.out.println("resumed");
            audioClip.setMicrosecondPosition(pauseTime);
            audioClip.start();
        }
    }

    public boolean isPlaying() {
        return (audioClip != null && audioClip.isRunning());
    }
}
