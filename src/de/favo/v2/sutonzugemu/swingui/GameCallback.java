package de.favo.v2.sutonzugemu.swingui;

/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
public interface GameCallback {
    void gotNewScore(int score);
    void gameEventOccurred(GameEvent event);

    enum GameEvent {
        IMPOSSIBLE_MOVE, DID_A_MOVE, TILE_SELECTED, ANIMATION_FINISHED
    }
}

