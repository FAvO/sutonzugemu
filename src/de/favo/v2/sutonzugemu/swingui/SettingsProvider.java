package de.favo.v2.sutonzugemu.swingui;

/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import de.favo.v2.sutonzugemu.logic.Logic;

import java.awt.*;

public class SettingsProvider {

    protected static double animationSpeed() { // speed per tile in seconds
        return 0.25d;
    }
    protected static int getFramesPerSecond() {
        return 30;
    }
    protected static int getGameViewPadding(Dimension gameSize) {
        return (int) (0.02d * gameSize.getWidth());
    }
    protected static int getTileSpaceWidth(Dimension gameSize) {
        return (int) ((gameSize.getWidth()-getGameViewPadding(gameSize) * 2) / Logic.FIELD_SIZE);
    }

    protected static int getTileSpaceHeight(Dimension gameSize) {
        return (int) ((gameSize.getHeight()-getGameViewPadding(gameSize) * 2) / Logic.FIELD_SIZE);
    }

    protected static int getTilePaddingSize(Dimension gameSize) {
        return (int) (0.005d * gameSize.getWidth());
    }

}
