package de.favo.v2.sutonzugemu.swingui;
/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import de.favo.v2.sutonzugemu.common.PrecisePoint;
import de.favo.v2.sutonzugemu.logic.GameEvent;
import de.favo.v2.sutonzugemu.logic.GameEventType;
import de.favo.v2.sutonzugemu.logic.Logic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import de.favo.v2.sutonzugemu.common.Point;

public class GameView extends JComponent implements MouseListener, ActionListener, ComponentListener {
    private final Logic logic;
    private final ImageProvider imageProvider;
    private final Vector<Tile> tiles;
    private Tile selectedTile;
    private final Timer timer;
    private final int tpf;
    private final Queue<GameEvent[]> gameEvents;
    private final AtomicBoolean gameEventPending;
    private final AtomicBoolean fieldIsTouchable;
    private Timer resizeFinishedTimer;
    private final GameCallback gameCallback;
    public GameView(GameCallback gameCallback, Dimension size) {
        super();
        this.setPreferredSize(size);
        this.setDoubleBuffered(true);
        this.setVisible(true);
        this.requestFocus();
        this.addMouseListener(this);
        this.addComponentListener(this);

        this.gameCallback = gameCallback;
        this.tpf = 1000 / SettingsProvider.getFramesPerSecond();
        this.timer = new Timer(this.tpf, this);
        this.gameEventPending = new AtomicBoolean(false);
        this.fieldIsTouchable = new AtomicBoolean(false);
        this.gameEvents = new ConcurrentLinkedQueue<>();
        this.logic = new Logic();
        this.tiles = new Vector<>(8*8+20);

        for (int y = 0; y < Logic.FIELD_SIZE; y++) {
            for (int x = 0; x < Logic.FIELD_SIZE; x++) {
                tiles.add(new Tile(logic.getGame()[y][x], new Point(x,y)));
            }
        }

        imageProvider = new ImageProvider( "default");
        imageProvider.loadImages(size);
        this.timer.start();
        this.fieldIsTouchable.set(true);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        BufferedImage img;
        Dimension gameSize = this.getSize();
        Dimension size;
        Point realLocation;
        PrecisePoint logicLocation;
        Tile tile1, tile2;
        GameEvent[] events;
        boolean anyPendingAnimation = false;

        if (gameEvents.size() > 0 && !gameEventPending.get()){
            events = gameEvents.poll();
            assert events != null;
            for (int i = 0; i < events.length; i++) {
                GameEvent event = events[i];
                switch (event.getReason()) {
                    case TILES_EXCHANGED -> {
                        tile1 = findTileForPosition(event.getPoint1());
                        tile2 = findTileForPosition(event.getPoint2());
                        assert tile1 != null;
                        assert tile2 != null;
                        tile1.setState(Tile.State.DEFAULT);
                        tile2.setState(Tile.State.DEFAULT);
                        tile1.animateToLocation(event.getPoint2());
                        tile2.animateToLocation(event.getPoint1());
                    }
                    case TILE_MOVED_TO -> {
                        tile1 = findTileForPosition(event.getPoint1());
                        assert tile1 != null;
                        tile1.animateToLocation(event.getPoint2());
                    }
                    case TILE_REMOVED -> this.tiles.remove(findTileForPosition(event.getPoint1()));
                    case NEW_TILE_ADDED -> {
                        tile1 = new Tile(event.getColor(), new Point(event.getPoint1().x(), -1 - (events.length - i)));
                        tile1.animateToLocation(event.getPoint1());
                        this.tiles.add(tile1);
                    }
                    case NEW_SCORE_REACHED -> gameCallback.gotNewScore(event.getScore());
                }
                if (!event.getReason().equals(GameEventType.NEW_SCORE_REACHED)) {
                    gameEventPending.set(true);
                    fieldIsTouchable.set(false);
                }
            }
        }

        g.drawImage(imageProvider.getBackground(),0,0, getSize().width, getSize().height, null);

        for (Tile tile:tiles) {
            img         = imageProvider.getTile(tile.getState(), tile.getColor());
            size        = new Dimension(SettingsProvider.getTileSpaceWidth(gameSize), SettingsProvider.getTileSpaceHeight(gameSize));

            if (tile.getState() == Tile.State.ANIMATED) {
                logicLocation = tile.getNextFrame();
                anyPendingAnimation = true;
            } else {
                logicLocation = tile.getLocation().toPrecisePoint();
            }
            realLocation = new Point(
                    (int)(SettingsProvider.getGameViewPadding(gameSize) + SettingsProvider.getTileSpaceWidth(gameSize)  * logicLocation.x()),
                    (int)(SettingsProvider.getGameViewPadding(gameSize) + SettingsProvider.getTileSpaceHeight(gameSize) * logicLocation.y()));

            g.drawImage(img, realLocation.x(), realLocation.y(), (int) size.getWidth(), (int) size.getHeight(), null);
        }
        if (!anyPendingAnimation) {
            gameEventPending.set(false);
        }
        if (!anyPendingAnimation && gameEvents.size() == 0 && !fieldIsTouchable.get()) {
            fieldIsTouchable.set(true);
            gameCallback.gameEventOccurred(GameCallback.GameEvent.ANIMATION_FINISHED);
        }
        Toolkit.getDefaultToolkit().sync();
    }

    private Point determineTileUnderMouse(MouseEvent mouseEvent) {
        int x = mouseEvent.getX(), y = mouseEvent.getY();

        if (x < SettingsProvider.getGameViewPadding(this.getSize()) || y < SettingsProvider.getGameViewPadding(this.getSize()))
            return new Point(-1,-1);

        x -= SettingsProvider.getGameViewPadding(this.getSize());
        y -= SettingsProvider.getGameViewPadding(this.getSize());

        x /= SettingsProvider.getTileSpaceWidth(this.getSize());
        y /= SettingsProvider.getTileSpaceHeight(this.getSize());

        if (x >= Logic.FIELD_SIZE || y >= Logic.FIELD_SIZE)
            return new Point(-1,-1);
        return new Point(x,y);
    }

    private Tile findTileForPosition(Point position) {
        for (Tile tile:tiles) {
            if (tile.getLocation().equals(position))
                return tile;
        }
        return null;
    }

    private void processMovement(Point firstTile, Point secondTile) {
        GameEvent[][] events;
        if (logic.isMovePossible(firstTile, secondTile)) {
            events = logic.exchange(firstTile, secondTile);
            Collections.addAll(gameEvents, events);
            gameCallback.gameEventOccurred(GameCallback.GameEvent.DID_A_MOVE);
        } else {
            gameCallback.gameEventOccurred(GameCallback.GameEvent.IMPOSSIBLE_MOVE);
        }
    }
    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        Point p = determineTileUnderMouse(mouseEvent);
        Tile clickedTile;
        if (!this.fieldIsTouchable.get())
            return;
        if (p.isValidPoint()) {
            clickedTile = findTileForPosition(p);
            if (clickedTile != null) {
                if (selectedTile == null) {
                    this.gameCallback.gameEventOccurred(GameCallback.GameEvent.TILE_SELECTED);
                    selectedTile = clickedTile;
                    clickedTile.setState(Tile.State.FOCUSED);
                } else {
                    this.gameCallback.gameEventOccurred(GameCallback.GameEvent.TILE_SELECTED);
                    processMovement(selectedTile.getLocation(), clickedTile.getLocation());
                    selectedTile = null;
                }
            } else {
                System.err.println("No tile found; where did it vanish?!");
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == this.timer) {
            long startTime = System.nanoTime();
            repaint();
            timer.setDelay(Math.max(0, this.tpf - (int) ((System.nanoTime() - startTime) / 1000)));
            timer.restart();
        }
        if (actionEvent.getSource() == this.resizeFinishedTimer) {
            this.resizeFinishedTimer.stop();
            this.resizeFinishedTimer = null;
            new Thread(() -> imageProvider.prepareImages(this.getSize())).start();
        }
    }

    @Override
    public void componentResized(ComponentEvent componentEvent) {
        if (this.resizeFinishedTimer == null)
        {   /* Start waiting for DELAY to elapse. */
            this.resizeFinishedTimer = new Timer(500,this);
            this.resizeFinishedTimer.start();
        }
        else
        {   /* Event came too soon, swallow it by resetting the timer.. */
            this.resizeFinishedTimer.restart();
        }
    }

    @Override
    public void componentMoved(ComponentEvent componentEvent) {

    }

    @Override
    public void componentShown(ComponentEvent componentEvent) {

    }

    @Override
    public void componentHidden(ComponentEvent componentEvent) {

    }

    public int getPossibleMoveAmount() {
        return this.logic.getPossibleMoves().size();
    }
}
