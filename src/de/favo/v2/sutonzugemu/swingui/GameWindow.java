package de.favo.v2.sutonzugemu.swingui;

/*  Sutōnzugēmu
    Copyright (C) 2022  Felix v. O.
    sutonzugemu@hyrrokkin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class GameWindow extends JFrame implements GameCallback {

    private static final String IS_MUSIC_PLAYING = "PREF_IS_MUSIC_PLAYING";
    private static final String LAST_SCORE = "PREF_LAST_SCORE";
    private static final String BEST_SCORE = "PREF_BEST_SCORE";
    private GameView gameView;
    private final JLabel scoreLabel;
    private final JLabel messageLabel;
    private final JMenuItem jMenuItemLastScore;
    private final JMenuItem jMenuItemBestScore;
    private final JPanel textBoard;
    private final AudioPlayer audioPlayer;
    private final Preferences preferences;
    private int lastScore = 0;

    public static void main(String[] args) throws UnsupportedLookAndFeelException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        GameWindow gameWindow = new GameWindow();
        gameWindow.setLocationRelativeTo(null);
        gameWindow.setVisible(true);
    }

    public GameWindow() {
        this.setTitle("ストーンズゲーム");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addWindowListener(new WindowListener() {
            @Override public void windowOpened(WindowEvent windowEvent) {}
            @Override public void windowClosing(WindowEvent windowEvent) {
                audioPlayer.stop();
            }
            @Override public void windowClosed(WindowEvent windowEvent) {}
            @Override public void windowIconified(WindowEvent windowEvent) {}
            @Override public void windowDeiconified(WindowEvent windowEvent) {}
            @Override public void windowActivated(WindowEvent windowEvent) {}
            @Override public void windowDeactivated(WindowEvent windowEvent) {}
        });
        this.preferences = Preferences.userNodeForPackage(this.getClass());
        this.audioPlayer = new AudioPlayer();

        Font labelFont      = new JLabel().getFont().deriveFont(Font.PLAIN, 17);
        Border labelBorder  = BorderFactory.createEmptyBorder(4,4,4,4);

        JMenuBar menuBar = new JMenuBar();

        JMenu jMenuFile = new JMenu("File");
        JMenuItem jMenuItemQuit = new JMenuItem("Quit...");
        jMenuItemQuit.addActionListener(actionEvent -> dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)));
        jMenuFile.add(jMenuItemQuit);
        menuBar.add(jMenuFile);

        JMenu jMenuStats = new JMenu("Statistics");
        jMenuItemLastScore = new JMenuItem("last score: ?");
        jMenuItemLastScore.setEnabled(false);
        jMenuStats.add(jMenuItemLastScore);
        jMenuItemBestScore = new JMenuItem("best score: ?");
        jMenuItemBestScore.setEnabled(false);
        jMenuStats.add(jMenuItemBestScore);
        menuBar.add(jMenuStats);

        updateStatistics();

        JMenu jMenuSettings = new JMenu("Settings");
        JCheckBoxMenuItem jCheckBoxMenuItemMusicPlaying = new JCheckBoxMenuItem("Music");
        jCheckBoxMenuItemMusicPlaying.setState(preferences.getBoolean(IS_MUSIC_PLAYING, true));
        jCheckBoxMenuItemMusicPlaying.addActionListener(actionEvent -> {
            preferences.putBoolean(IS_MUSIC_PLAYING, jCheckBoxMenuItemMusicPlaying.getState());
            save();
            if (jCheckBoxMenuItemMusicPlaying.getState()) {
                if (!audioPlayer.isPlaying()) {
                    try {
                        audioPlayer.play();
                    } catch (AudioPlaybackFailedException e) {
                        jCheckBoxMenuItemMusicPlaying.setState(false);
                        jCheckBoxMenuItemMusicPlaying.setEnabled(false);
                    }
                }
            } else {
                audioPlayer.stop();
            }
        });
        jMenuSettings.add(jCheckBoxMenuItemMusicPlaying);

        menuBar.add(jMenuSettings);

        menuBar.add(Box.createHorizontalGlue());
        JMenu jMenuHelp = new JMenu("Help");
        JMenuItem jMenuItemHelp = new JMenuItem("Help");
        jMenuItemHelp.addActionListener(actionEvent -> JOptionPane.showMessageDialog(this, """

                There isn't any help right now.                \t
                We are a kind of sorry.

                """, "we are working on that…", JOptionPane.WARNING_MESSAGE));
        JMenuItem jMenuItemAbout = new JMenuItem("About");
        jMenuItemAbout.addActionListener(actionEvent -> JOptionPane.showMessageDialog(this, """
                === About ストーンズゲーム (Sutōnzugēmu) ===
                
                This is a little game.           \t
                It is licenced under the GPLv3 and
                contains music by UniqOfficial.
                
                If you have any questions, feel free to send me an email to
                sutonzugemu@hyrrokkin.eu.
                
                Greetings,
                Felix v. O.
                """, "about ストーンズゲーム", JOptionPane.PLAIN_MESSAGE));
        jMenuHelp.add(jMenuItemHelp);
        jMenuHelp.add(jMenuItemAbout);
        menuBar.add(jMenuHelp);
        setJMenuBar(menuBar);

        createGameView();

        textBoard = new JPanel();
        textBoard.setLayout(new BorderLayout());
        getContentPane().add(textBoard, BorderLayout.SOUTH);

        messageLabel = new JLabel("ready.", JLabel.LEFT);
        messageLabel.setBackground(Color.CYAN);
        messageLabel.setFont(labelFont);
        messageLabel.setBorder(labelBorder);
        textBoard.add(messageLabel, BorderLayout.WEST);

        scoreLabel = new JLabel("" + 0 + " score", JLabel.RIGHT);
        scoreLabel.setBorder(labelBorder);
        scoreLabel.setFont(labelFont);
        textBoard.add(scoreLabel, BorderLayout.EAST);
        pack();
        if (jCheckBoxMenuItemMusicPlaying.getState()) {
            try {
                this.audioPlayer.play();
            } catch (AudioPlaybackFailedException e) {
                jCheckBoxMenuItemMusicPlaying.setState(false);
                jCheckBoxMenuItemMusicPlaying.setEnabled(false);
            }
        }
    }

    private void updateStatistics() {
        int lastScore = preferences.getInt(LAST_SCORE, -1);
        int bestScore = preferences.getInt(BEST_SCORE, -1);
        jMenuItemLastScore.setText("last score: " + (lastScore == -1 ? "?" : lastScore));
        jMenuItemBestScore.setText("best score: " + (bestScore == -1 ? "?" : bestScore));
    }

    private void save() {
        try {
            preferences.flush();
        } catch (BackingStoreException e) {
            throw new RuntimeException(e);
        }
    }

    private void createGameView() {
        gameView = new GameView(this, new Dimension(800,600));
        gameView.setBackground(Color.BLACK);
        getContentPane().add(gameView, BorderLayout.CENTER);
    }

    @Override
    public void gotNewScore(int score) {
        this.lastScore = score;
        scoreLabel.setText("" + score + " score");
    }

    @Override
    public void gameEventOccurred(GameEvent gameEvent) {
        messageLabel.setText("");
        if (gameEvent.equals(GameEvent.IMPOSSIBLE_MOVE)) {
            messageLabel.setText("impossible move!");
        } else if (gameEvent.equals(GameEvent.ANIMATION_FINISHED)) {
            int cnt = this.gameView.getPossibleMoveAmount();
            if (cnt == 0) {
                gameFinished();
            } else {
                messageLabel.setText("(" + cnt + " possible moves)");
            }
        }
    }

    private void gameFinished() {
        SwingUtilities.invokeLater(() -> {
            JOptionPane.showMessageDialog(GameWindow.this, ("You've finished the game with " + lastScore + " points.\nGreat!\n"), "no more moves possible!", JOptionPane.INFORMATION_MESSAGE);
            getContentPane().remove(gameView);
            getContentPane().revalidate();
            getContentPane().repaint();
            gameView = null;
            createGameView();
        });
        preferences.putInt(LAST_SCORE, lastScore);
        if (preferences.getInt(BEST_SCORE, 0) < lastScore) {
            preferences.putInt(BEST_SCORE, lastScore);
        }
        save();
        SwingUtilities.invokeLater(this::updateStatistics);
    }
}
